function generateError() {
    return `
    <div class="alert alert-danger" role="alert">
         Oops! Something went wrong, please try again.
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';       
    
    try {
        const response = await fetch(url);
                
        if (!response.ok)   {
            throw new Error('Response not ok')
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                const option = document.createElement('option');            
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData.entries()));
                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "POST",
                    body: json,
                    headers: {
                    'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();                
                    const newConference = await response.json();                
                };
            })
        }   
    } catch (e) {
        console.error(e);
        const html = generateError();
        const column = document.querySelector('.row');
        column.innerHTML += html;
    
});
