function createCard(name, description, pictureUrl, formattedStart, formattedEnd, location) {
    return `
    <div class="col">
        <div class="card border-dark text-center shadow-lg p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${formattedStart} - ${formattedEnd}
            </div>
        </div>
    </div>
    `;
  }
  
  function generateError() {
    return `
    <div class="alert alert-danger" role="alert">
         Oops! Something went wrong, please try again.
    </div>
    `;
  }
  
  
  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error('Response not ok');
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const starts = details.conference.starts;
            const startDate = new Date(starts);
            const formattedStart = `${startDate.getMonth()+1}-${startDate.getDate()}-${startDate.getFullYear()}`;
            const ends = details.conference.ends;
            const endDate = new Date(ends);
            const formattedEnd = `${endDate.getMonth()+1}-${endDate.getDate()}-${endDate.getFullYear()}`;


            const html = createCard(title, description, pictureUrl, formattedStart, formattedEnd, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
        console.error(e);
        const html = generateError();
        const column = document.querySelector('.row');
        column.innerHTML += html;
    

        // Figure out what to do if an error is raised
      }
  
  });